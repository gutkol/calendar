﻿using Calendar.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Calendar
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Dodanie filtru SSL dla bezpiecznego polaczenia HTTPS - w IIS dziala tylko w wersji produkcyjnej
            //GlobalFilters.Filters.Add(new RequireHttpsAttribute());
            ViewEngines.Engines.Add(new LocalizationViewEngine());
        }
    }
}
