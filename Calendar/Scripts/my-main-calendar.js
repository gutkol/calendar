﻿$(document).ready(function () {
    var defaultView = (localStorage.getItem("fcDefaultView") !== null ? localStorage.getItem("fcDefaultView") : "month");

    var selectedEvent = null;
    var selectedEventType = null;
    var titleChoise;
    var events = [];

    var canSave = true;
    var canSaveEventType = true;

    fetchCalendarEvents();
    function fetchCalendarEvents() {
        events = [];
        $.ajax({
            type: "GET",
            url: "/Sheet/GetEvents",
            success: function (data) {
                $.each(data, function (i, v) {
                    events.push({
                        scheduleID: v.ScheduleID,
                        eventTypeID: v.EventTypeID,
                        title: v.Subject,
                        start: moment(v.StartDate),
                        end: moment(v.EndDate),
                        desciption: v.Description,
                        event: v.EventName,
                        color: v.EventColor,
                        textColor: pickTextColorBasedOnBgColorSimple(v.EventColor, '#FFFFFF', '#000000'),
                        attention: v.Attention,
                        files: v.Files
                    });
                });
                generateCalendar(events);
            },
            error: function (error) {
                alert(error);
            }
        });
    }

    function generateCalendar(events) {
        $('#calendar').fullCalendar('destroy');
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },

            defaultDate: new Date(),
            locale: 'pl',
            editable: true,
            allDaySlot: false,
            slotMinutes: 15,
            color: null,
            textColor: null,
            nowIndicator: true,
            events: events,
            defaultView: defaultView,
            viewRender: function (view, element) {
                localStorage.setItem("fcDefaultView", view.name);
            },
            eventClick: function (calEvent, jsEvent, view) {
                selectedEvent = calEvent;
                $('#myModal #eventTitle').text(calEvent.title);
                var $description = $('<div/>');
                $description.append($('<p/>').html('<b>Start: </b>' + calEvent.start.format("DD MMMM YYYY")
                    + ('<b> Godzina: </b>' + calEvent.start.format("HH:mm"))));
                if (calEvent.end !== null) {
                    $description.append($('<p/>').html('<b>Koniec: </b>' + calEvent.end.format("DD MMMM YYYY")
                        + ('<b> Godzina: </b>' + calEvent.end.format("HH:mm"))));
                }
                $description.append($('<p/>').html('<b>Typ wydarzenia: </b>' + calEvent.event));
                if (calEvent.desciption !== null)
                    $description.append($('<p/>').html('<b>Opis: </b>' + calEvent.desciption));
                else
                    $description.append($('<p/>').html('<b>Opis: </b>' + "(brak)"));

                if (calEvent.attention !== null)
                    $description.append($('<p/>').html('<b>Uwagi: </b>' + calEvent.attention));
                else
                    $description.append($('<p/>').html('<b>Uwagi: </b>' + "(brak)"));

                if (calEvent.files.length !== 0) {
                    $description.append($('<p/>').html('<b> Pliki: </b>'));
                    $description.append($('<table/>'));

                    for (var itemFile = 0; itemFile < calEvent.files.length; itemFile++) {
                        var sizePrecission = calEvent.files[itemFile].FileSize / 1024;
                        if ((sizePrecission / 1024).toFixed(2).indexOf(0) > 0) {
                            sizePrecission = sizePrecission.toFixed(2) + 'MB';
                        }
                        else {
                            sizePrecission = sizePrecission.toFixed(2) + 'KB';
                        }
                        
                        $description.append($('<tr/>').html('<td class="border-0">' + calEvent.files[itemFile].FileName + '</td>'
                            + '<td class="border-0" style="padding-left: 10px;">' + sizePrecission + '</td>'
                            + '<td class="border-0" style="padding-left: 10px;">'
                            + '<a class="btn btn-sm btn-secondary" href="/Sheet/FileDownload?fileID='
                            + calEvent.files[itemFile].FileID + '">Pobierz</a></td>'
                            + '<td class="border-0" style="padding-left: 10px;">'
                            + '<a class="btn btn-sm btn-danger" href="/Sheet/FileRemove?fileID='
                            + calEvent.files[itemFile].FileID + '">Usuń</a></td>'));
                    }
                }
                else
                    $description.append($('<p/>').html('<b> Pliki: </b>' + "(brak)"));
                
                $('#myModal #eventDetails').empty().html($description);

                $('#myModal').modal();
            },
            selectable: true,
            select: function (start, end) {
                selectedEvent = {
                    scheduleID: null,
                    eventTypeID: typesOfEvents[0].eventTypeID,
                    title: null,
                    start: start,
                    end: end,
                    desciption: null,
                    attention: null,
                    files: $('#txtFiles').val("")
                };
                canSave = true;
                titleChoise = true;
                openEditModal(titleChoise);
                $('#calendar').fullCalendar('unselect');
            },
            eventDrop: function (event, delta, revertFunc) {
                interactiveMovingEvent(event);
            },
            eventResize: function (event, delta, revertFunc) {
                interactiveMovingEvent(event);
            }
        });
    }

    $('#editButton').click(function () {
        titleChoise = false;
        canSave = true;
        openEditModal(titleChoise);
    });

    function interactiveMovingEvent(event) {
        var dataEvent = {
            ScheduleID: event.scheduleID,
            Subject: event.title,
            StartDate: event.start.format('YYYY.MM.DD HH:mm'),
            EndDate: event.end.format('YYYY.MM.DD HH:mm'),
            Description: event.desciption,
            EventTypeID: event.eventTypeID,
            EventName: event.event,
            EventColor: event.color,
            Attention: event.attention
        };
        saveEvent(dataEvent);
    }

    function openEditModal(titleChoise) {
        $('#editModal').modal();
        if (titleChoise)
            $('#modalEditTitle').html("Dodawanie wydarzenia");
        else
            $('#modalEditTitle').html("Edytowanie wydarzenia");

        if (selectedEvent !== null) {
            $('#eventScheduleID').val(selectedEvent.scheduleID);
            $('#txtSubject').val(selectedEvent.title);
            $('#dtStart').val(selectedEvent.start.format('YYYY.MM.DD HH:mm'));
            $('#dtEnd').val(selectedEvent.end.format('YYYY.MM.DD HH:mm'));
            $('#txtDescription').val(selectedEvent.desciption);
            $('#txtEventTypeID').val(selectedEvent.eventTypeID);
            $('#txtAttention').val(selectedEvent.attention);
        }
        //////////////////////////////// WPISYWAC TUTAJ DO UPUSZCZANIA PLIKOW - TEST /////////////////////////////////
        //var dropzone = document.getElementById('dropzone');

        //dropzone.ondrop = function (ev) {
        //    ev.preventDefault();
        //    $('dropzone').removeClass('dropzone-active');

        //    var files = ev.dataTransfer.files;
        //    if (!files) return;

        //    var testowy = function (files) {
        //        var dataForm = new formData();

        //        for (var i = 0; x < files.length; i++) {
        //            dataForm.append(files[i].name, files[i]);
        //        }
        //        $.ajax({
        //            type: "POST",
        //            url: '/Sheet/UploadFiles',
        //            contentType: false,
        //            data: dataForm,
        //            success: function (data) {
        //                alert(data);
        //            },
        //            error: function (data) {
        //                alert(data);
        //            }
        //        })
        //    }
        //}
        //dropzone.ondragover = function () {
        //    $('#dropzone').addClass('dropzone-active');
        //}
        //dropzone.ondragleave = function () {
        //    $('#dropzone').removeClass('dropzone-active');
        //}
        /////////////////////////////////////////////// KONIEC TESTU ////////////////////////////////////

        $('#confirmEditButton').click(function () {
            //var formData = new FormData();
            var totalFiles = document.getElementById('txtFiles');
            var fileList = [];
            for (i = 0; i < totalFiles.files.length; i++) {
                if (totalFiles.files[i] !== null) {
                    var buffer = [];
                    var reader = new FileReader();
                    reader.onload = function (evt) {
                        for (byte = 0; byte < reader.result.byteLength; byte++)
                            buffer[byte] = new Uint8Array(reader.result)[byte];
                        console.log(reader.result);
                    };

                    reader.onloadend = function (evt) {
                        if (reader.readyState !== FileReader.DONE)
                            return;
                    };

                    reader.readAsArrayBuffer(totalFiles.files[i]);

                    console.log(buffer);
                    fileList.push({
                        FileName: totalFiles.files[i].name, FileContentType: totalFiles.files[i].type,
                        FileSize: totalFiles.files[i].size, FileContent: buffer
                    });
                }
            }

            var dataEvent = {
                ScheduleID: $('#eventScheduleID').val(),
                Subject: $('#txtSubject').val(),
                StartDate: $('#dtStart').val(),
                EndDate: $('#dtEnd').val(),
                Description: $('#txtDescription').val(),
                EventTypeID: $('#txtEventTypeID').val(),
                EventColor: "#0050f0",
                Attention: $('#txtAttention').val(),
                Files: fileList
            };

            //formData.append('calendarEvent', dataEvent);
            //for (var i = 0; i < totalFiles.length; i++) {
            //    var file = document.getElementById('txtFiles').files[i];
            //    formData.append('inputFile', file);
            //}
            if (canSave) {
                canSave = false;
                saveEvent(dataEvent);
            }
            //$.ajax({
            //    type: "POST",
            //    url: "/Sheet/SaveEvent",
            //    data: formData, 
            //    datatype: 'json',
            //    processData: false,
            //    success: function (data) {
            //        if (data.status) {
            //            fetchCalendarEvents();
            //            $('#editModal').modal('hide');
            //            $('#myModal').modal('hide');
            //        }
            //    },
            //    error: function () {
            //        alert("Błąd podczas edytowania wydarzenia");
            //    }
            //})
        });
        $('#cancelEditButton').click(function () {
            $('#editModal').modal('hide');
        });
    }

    function saveEvent(dataEvent) {
        var bar = $('.progress-bar');
        var percent = $('.progress-bar');

        $.ajax({
            type: "POST",
            url: "/Sheet/SaveEvent",
            //beforeSend: function () {
            //    var percentValue = '0%';
            //    bar.width(percentValue);
            //    percent.html(percentValue);
            //},
            //uploadProgress: function () {
            //    var percentValue = percentComplete + '%';
            //    bar.width(percentValue);
            //    percent.html(percentValue);
            //},
            data: dataEvent,
            success: function (data) {
                var percentValue = '100%';
                bar.width(percentValue);
                percent.html(percentValue);
                if (data.status) {
                    fetchCalendarEvents();
                    $('#editModal').modal('hide');
                    $('#myModal').modal('hide');
                }
            },
            error: function () {
                alert("Błąd podczas edytowania wydarzenia");
            }
        });
    }

    $('#deleteModal').on('hidden.bs.modal', function (e) {
        $('#myModal').css("opacity", "1");
    });

    $('#deleteButton').click(function () {
        $('#deleteModal').modal();
        $('#myModal').css("opacity", "0.5");

        $('#confirmDeleteButton').click(function () {
            if (selectedEvent !== null) {
                $.ajax({
                    type: "POST",
                    url: "/Sheet/DeleteEvent",
                    data: { 'scheduleID': selectedEvent.scheduleID },
                    success: function (data) {
                        if (data.status) {
                            fetchCalendarEvents();
                            $('#deleteModal').modal('hide');
                            $('#myModal').modal('hide');
                        }
                    },
                    error: function () {
                        alert("Błąd podczas usuwania wydarzenia");
                    }
                });
            }
        });
        $('#cancelDeleteButton').click(function () {
            $('#deleteModal').modal('hide');
        });
    });

    colorPicker();
    function colorPicker() {
        $('#colorpicker').farbtastic('#color');
    }

    var typesOfEvents = [];
    getManageEvents();
    function getManageEvents() {
        typesOfEvents = [];
        $.ajax({
            type: "GET",
            url: "/Sheet/GetAllEventsToManage",
            success: function (data) {
                $.each(data, function (i, v) {
                    typesOfEvents.push({
                        eventTypeID: v.EventTypeID,
                        eventName: v.EventName,
                        eventColor: v.EventColor
                    });
                });
                generateEventsType(typesOfEvents);
            },
            error: function (error) {
                alert(error);
            }
        });
    }

    var tableBody = null;
    function generateEventsType(typesOfEvents) {
        if (typesOfEvents === null) {
            console.log("costam");
        }
        else {
            tableBody = null;
            $('#txtEventTypeID').empty();
            for (var i = 0; i < typesOfEvents.length; i++) {
                tableBody += '<tr id="' + i + '">';
                
                var color = typesOfEvents[i].eventColor;
                var contrast = pickTextColorBasedOnBgColorSimple(color, '#FFFFFF', '#000000');

                tableBody += '<td class="border-0">';
                tableBody += '<button class="btn btn-sm" id="manageEventColor" type="button" style="width: 100%; margin-bottom: -10px; background-color:' +
                    typesOfEvents[i].eventColor + '; color: ' + contrast + ';">';
                tableBody += '<input class="text-hide" id="manageEventID" name="manageEventID" type="text" value:"' +
                    typesOfEvents[i].eventTypeID + '"/>';
                tableBody += '<span class="font-weight-bold" id="manageEventName">' + typesOfEvents[i].eventName + '</span>';
                tableBody += '</button></td></tr>';

                $('#txtEventTypeID').append('<option value="' + typesOfEvents[i].eventTypeID + '">'
                    + typesOfEvents[i].eventName + '</option>');
            }
            $('#tbodyForManageEventTypes').html(tableBody);
        }
    }

    $("table").delegate("tr", "click", function () {
        var id = $(this).attr('id');
        selectedEventType = typesOfEvents[id];
        $('#eventTypeModal #eventTypeName').html(selectedEventType.eventName);
        var color = pickTextColorBasedOnBgColorSimple(selectedEventType.eventColor, '#FFFFFF', '#000000');
        var $detailsOfEventType = $('<div/>');
        $detailsOfEventType.append($('<p/>').html('<b>Kolor wydarzenia: </b>' + selectedEventType.eventColor));
        $detailsOfEventType.append($('<p/>').html('<b>Kolor tekstu: </b>' + color));

        $('#eventTypeModal #eventTypeDetails').empty().html($detailsOfEventType);
        $('#eventTypeModal').modal();

        canSaveEventType = true;
        $('#editEventTypeButton').click(function () {
            $('#editModalEventType').modal();
            $('#editModalEventType #colorpicker').farbtastic('#editModalEventType #color');

            if (selectedEventType !== null) {
                $('#editModalEventType #txtAddEventName').val(selectedEventType.eventName);
                $('#editModalEventType #color').val(selectedEventType.eventColor);
            }
            $('#editModalEventType #confirmEditButton').click(function () {
                var eventType = {
                    EventTypeID: selectedEventType.eventTypeID,
                    EventName: $('#editModalEventType #txtAddEventName').val(),
                    EventColor: $('#editModalEventType #color').val()
                };
                if (canSaveEventType) {
                    canSaveEventType = false;
                    saveEventType(eventType);
                }
            });
            $('#editModalEventType #cancelEditButton').click(function () {
                $('#editModalEventType').modal('hide');
            });
        });
        $('#deleteEventTypeButton').click(function () {
            if (canSaveEventType) {
                canSaveEventType = false;
                $.ajax({
                    type: "POST",
                    url: "/Sheet/DeleteEventType",
                    data: { 'deleteEventTypeID': selectedEventType.eventTypeID },
                    success: function (data) {
                        if (data.status) {
                            getManageEvents();
                            fetchCalendarEvents();
                            $('#eventTypeModal').modal('hide');
                        }
                        else
                            alert("Nie można usunąć podstawowego typu wydarzenia");
                    },
                    error: function () {
                        alert("Błąd usuwania eventu");
                    }
                });
            }
        });
    });

    $('#addEventName').click(function () {
        var eventType = {
            EventName: $('#txtAddEventName').val(),
            EventColor: $('#color').val()
        };
        if (eventType.EventName === null) {
            Error: alert("Pole nie może być puste");
        }
        saveEventType(eventType);
    });

    function saveEventType(eventType) {
        $.ajax({
            type: "POST",
            url: "/Sheet/SaveEventType",
            data: eventType,
            success: function (data) {
                if (data.status) {
                    $('#txtAddEventName').val(null);
                    $('#editModalEventType').modal('hide');
                    $('#eventTypeModal').modal('hide');
                    fetchCalendarEvents();
                    getManageEvents();
                }
                else {
                    $('#txtAddEventName').val(null);
                    alert("Taki typ wydarzenia już istnieje lub wystąpiła próba edycji podstawowego elementu");
                    $('#editModalEventType').modal('hide');
                }
            },
            error: function () {
                alert("Błąd dodawania typu wydarzenia");
            }
        });
    }

    function pickTextColorBasedOnBgColorSimple(bgColor, lightColor, darkColor) {
        var color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
        var r = parseInt(color.substring(0, 2), 16); // hexToR
        var g = parseInt(color.substring(2, 4), 16); // hexToG
        var b = parseInt(color.substring(4, 6), 16); // hexToB
        return (((r * 0.299) + (g * 0.587) + (b * 0.114)) > 186) ?
            darkColor : lightColor;
    }

});