namespace Calendar
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class CalendarContext : DbContext
    {
        // Your context has been configured to use a 'CalendarContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Calendar.CalendarContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CalendarContext' 
        // connection string in the application configuration file.
        public CalendarContext()
            : base("name=CalendarContext")
        {
            var checkEventNameOther = EventTypes.Where(x => x.EventName == "Inne").FirstOrDefault();
            var checkEventNameMeet = EventTypes.Where(x => x.EventName == "Spotkanie").FirstOrDefault();
            var checkEventNameBirthday = EventTypes.Where(x => x.EventName == "Urodziny").FirstOrDefault();

            if(checkEventNameOther == null && checkEventNameMeet == null && checkEventNameBirthday == null)
            {
                EventTypes.Add(new EventType()
                {
                    EventName = "Inne",
                    EventColor = "#0050f0"
                });
                EventTypes.Add(new EventType()
                {
                    EventName = "Spotkanie",
                    EventColor = "#fff74d"
                });
                EventTypes.Add(new EventType()
                {
                    EventName = "Urodziny",
                    EventColor = "#1fad2e"
                });
                SaveChanges();
            }
            
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<UploadedFile> UploadedFiles { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<ScheduleUploadedFile> ScheduleUploadedFiles { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class Schedule
    {
        public Schedule()
        {
            Realization = false;
        }

        [Key]
        public int ScheduleID { get; set; }
        public int UserID { get; set; }
        public int EventTypeID { get; set; }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string Attention { get; set; }
        public bool Realization { get; set; }

        public User User { get; set; }
        public virtual ICollection<ScheduleUploadedFile> ScheduleUploadedFiles { get; set; }
        public EventType EventType { get; set; }
    }

    public class User
    {
        [Key]
        public int UserID { get; set; }
        public int AccountID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }

        public Account Account { get; set; }
        public List<Schedule> Schedule { get; set; }
        public List<EventType> EventType { get; set; }
    }

    public class Account
    {
        [Key]
        public int AccountID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public List<User> User { get; set; }
    }

    public class UploadedFile
    {

        [Key]
        public int FileID { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string ContentType { get; set; }
        public string FileExtension { get; set; }
        public byte[] FileContent { get; set; }

        public virtual ICollection<ScheduleUploadedFile> ScheduleUploadedFiles { get; set; }
    }

    public class EventType
    {
        [Key]
        public int EventTypeID { get; set; }
        public int? UserID { get; set; }
        public string EventName { get; set; }
        public string EventColor { get; set; }

        public List<Schedule> Schedule { get; set; }
    }

    public class ScheduleUploadedFile
    {
        [Key, Column(Order = 0)]
        public int ScheduleID { get; set; }
        [Key, Column(Order = 1)]
        public int FileID { get; set; }

        public virtual Schedule Schedule { get; set; }
        public virtual UploadedFile UploadedFile { get; set; }
    }
}