﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Calendar.Infrastructure
{
    public class LocalizationViewEngine : RazorViewEngine
    {
        public LocalizationViewEngine()
        {
            base.ViewLocationFormats = new string[]
            {
                "~/Features/{1}/Views/{0}.cshtml"
            };
        }
    }
}