﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calendar.Infrastructure
{
    public class Happening
    {
        public int ScheduleID { get; set; }
        public int EventTypeID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string EventName { get; set; }
        public string EventColor { get; set; }
        public string Attention { get; set; }
        public List<FileParameter> Files { get; set; }
    }

    public class FileParameter
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string FileContentType { get; set; }
        public int[] FileContent { get; set; }
    }
}