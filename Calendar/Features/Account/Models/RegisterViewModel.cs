﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Features.Account.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Proszę podać imię.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Proszę podać nazwisko.")]
        public string Surname { get; set; }

        [Phone]
        [Required(ErrorMessage = "Proszę podać numer telefonu.")]
        [RegularExpression(@"^([0-9]{3})[ \s]\ ?([0-9]{3})[ \s]\ ?([0-9]{3})$",
            ErrorMessage = "Numer musi być w formacie: 123 123 123")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Proszę podać adres e-mail.")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Proszę podać prawidłowy adres e-mail.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Proszę podać hasło.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Proszę potwierdzić hasło.")]
        [Compare("Password", ErrorMessage = "Potwierdzone hasło nie pasuje do hasła.")]
        public string ConfirmPassword { get; set; }
    }
}