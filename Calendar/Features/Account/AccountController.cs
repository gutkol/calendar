﻿using Calendar.Features.Account.Models;
using Calendar.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Calendar.Features.Account
{
    public class AccountController : Controller
    {
        private readonly CalendarContext context = new CalendarContext();

        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            string loginCookie = HttpContext.User.Identity.Name;
            
            if (loginCookie != "")
            {
                var loginAcc = context.Accounts.Where(x => x.Email == loginCookie).FirstOrDefault();

                if (loginAcc == null)
                {
                    FormsAuthentication.SignOut();
                    return View();
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(loginAcc.Email, false);

                    var userAcc = context.Users.Where(x => x.AccountID == loginAcc.AccountID).FirstOrDefault();

                    Session["Name"] = userAcc.Name.ToString();

                    if (ReturnUrl == null)
                        return RedirectToAction("Schedule", "Sheet");
                    else
                        return RedirectToAction(ReturnUrl);
                }  
            }
            else
                return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel loginViewModel, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                var loginAcc = context.Accounts.Where(x => x.Email == loginViewModel.Email).FirstOrDefault();

                if(loginAcc != null)
                {
                    string decryptedPassword = Cipher.Decrypt(loginAcc.Password, loginViewModel.Password);

                    if (loginAcc.Email != null && Cipher.plainText == decryptedPassword)
                    {
                        FormsAuthentication.SetAuthCookie(loginViewModel.Email, loginViewModel.Remember);

                        var userAcc = context.Users.Where(x => x.AccountID == loginAcc.AccountID).FirstOrDefault();

                        Session["Name"] = userAcc.Name.ToString();

                        if (ReturnUrl == null)
                            return RedirectToAction("Schedule", "Sheet");
                        else
                            return Redirect(ReturnUrl);
                    }
                    else
                    {
                        ViewBag.Error = "Źle wprowadzono adres email lub hasło";
                        return View(loginViewModel);
                    }
                }
                else
                {
                    ViewBag.Error = "Źle wprowadzono adres email lub hasło";
                    return View(loginViewModel);
                }

                
            }
            else
            {
                return View(loginViewModel);
            }
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        public JsonResult CheckExistedEmail(string userdata)
        {
            var accountEmail = context.Accounts.Where(x => x.Email == userdata).FirstOrDefault();

            if (accountEmail != null)
            {
                return Json(1);
            }
            else
                return Json(0);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                context.Users.Add(new Calendar.User() {
                    Name = registerViewModel.Name,
                    Surname = registerViewModel.Surname,
                    PhoneNumber = registerViewModel.PhoneNumber
                });

                string cipherText = Cipher.Encrypt(registerViewModel.Password);

                context.Accounts.Add(new Calendar.Account() {
                    Email = registerViewModel.Email,
                    Password = cipherText
                });

                context.SaveChanges();

                ViewBag.Register = registerViewModel.Name + " " + registerViewModel.Surname;

                ModelState.Clear();

                return View();
            }
            else
                return View(registerViewModel);
        }
    }
}