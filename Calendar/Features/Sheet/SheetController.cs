﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Ical.Net.DataTypes;
using Ical.Net.Interfaces;
using Ical.Net.Interfaces.Components;
using System.Data;
using Ical.Net.Serialization.iCalendar.Serializers;
using Calendar.Infrastructure;

namespace Calendar.Features.Sheet
{
    [Authorize]
    public class SheetController : Controller
    {
        private readonly CalendarContext context = new CalendarContext();
        
        public ActionResult Schedule()
        {
            GetAllEventsToManage();
            ViewBag.RedundantRecord = TempData["RedundantRecord"];
            return View();
        }

        [HttpGet]
        public JsonResult GetEvents()
        {
            //TO PONIZEJ STOSOWAC DO ODCZYTYWANIA KALENDARZA, FOREACH MUSI BYC DO WYSWIETLENIA DANYCH Z INNEH TABELI
            

            (var loginAcc, var userAcc) = GetInfoFromCookie();

            //var ScheduleFiles = context.Schedules.Where(x => x.UserID == userAcc.UserID);
            //ScheduleFiles.SelectMany(x => x.ScheduleUploadedFiles).ToList()
            //    .ForEach(z => { z.Schedule.EventType = context.EventTypes.Where(l => l.EventTypeID == z.Schedule.EventTypeID).FirstOrDefault(); });
            //foreach (var inside in ScheduleFiles)
            //{
            //    inside.ScheduleUploadedFiles.Select(x => x.UploadedFile.FileName);
            //}

            var events = context.Schedules.Where(y => y.UserID == userAcc.UserID).ToList();
            events.Where(y => y.ScheduleUploadedFiles.Any());
            events.ForEach(x => { x.EventType = context.EventTypes.Where( z => z.EventTypeID == x.EventTypeID).FirstOrDefault(); });

            List<Happening> dataHappenings = new List<Happening>();
            List<FileParameter> fileParameters = null;

            foreach (var record in events)
            {
                fileParameters = new List<FileParameter>();
                if (record.ScheduleUploadedFiles.Count > 0)
                {
                    foreach (var fileRecord in record.ScheduleUploadedFiles)
                    {
                        fileParameters.Add(new FileParameter()
                        {
                            FileID = fileRecord.UploadedFile.FileID,
                            FileName = fileRecord.UploadedFile.FileName,
                            FileSize = fileRecord.UploadedFile.FileSize,
                            FileContentType = fileRecord.UploadedFile.ContentType
                        });
                    }
                }

                dataHappenings.Add(new Happening()
                {
                    ScheduleID = record.ScheduleID,
                    EventTypeID = record.EventTypeID,
                    Subject = record.Subject,
                    Description = record.Description,
                    StartDate = record.StartDate,
                    EndDate = record.EndDate,
                    EventName = record.EventType.EventName,
                    EventColor = record.EventType.EventColor,
                    Attention = record.Attention,
                    Files = fileParameters
                });
            }

            return new JsonResult { Data = dataHappenings, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [HttpPost]
        public JsonResult SaveEvent(Happening calendarEvent)
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();
            bool status = false;
            var accountSchedule = context.Schedules.Find(calendarEvent.ScheduleID);
            
            bool realization = false;

            if (calendarEvent.EndDate < DateTime.Now)
                realization = true;

            if (accountSchedule != null)
            {
                if(calendarEvent.Files != null)
                {
                    if (calendarEvent.Files.Count > 0)
                    {
                        foreach (var file in calendarEvent.Files)
                        {
                            if (file.FileContent != null)
                            {
                                var fileSchedule = (new UploadedFile()
                                {
                                    FileName = file.FileName,
                                    FileSize = file.FileSize,
                                    ContentType = file.FileContentType,
                                    FileExtension = Path.GetExtension(file.FileName),
                                    FileContent = file.FileContent.Select(x => (byte)x).ToArray()
                                });
                                accountSchedule.ScheduleUploadedFiles.Add(new ScheduleUploadedFile()
                                {
                                    UploadedFile = fileSchedule
                                });
                            }
                        }
                    }
                }

                accountSchedule.EventTypeID = calendarEvent.EventTypeID;
                accountSchedule.Subject = calendarEvent.Subject;
                accountSchedule.StartDate = calendarEvent.StartDate;
                accountSchedule.EndDate = calendarEvent.EndDate;
                accountSchedule.Description = calendarEvent.Description;
                accountSchedule.Attention = calendarEvent.Attention;
                accountSchedule.Realization = realization;
                
                context.Entry(accountSchedule).State = System.Data.Entity.EntityState.Modified;

                status = true;
            }
            else
            {
                if (calendarEvent.Subject != null)
                {
                    Schedule schedule = new Schedule
                    {
                        UserID = userAcc.UserID,
                        EventTypeID = calendarEvent.EventTypeID,
                        Subject = calendarEvent.Subject,
                        StartDate = calendarEvent.StartDate,
                        EndDate = calendarEvent.EndDate,
                        Description = calendarEvent.Description,
                        Attention = calendarEvent.Attention,
                        Realization = realization
                    };

                    if (calendarEvent.Files == null)
                    {
                        context.Schedules.Add(schedule);
                    }
                    else
                    {
                        foreach (var file in calendarEvent.Files)
                        {
                            UploadedFile uploadedFile = new UploadedFile
                            {
                                FileName = file.FileName,
                                FileSize = file.FileSize,
                                ContentType = file.FileContentType,
                                FileExtension = Path.GetExtension(file.FileContentType),
                                FileContent = file.FileContent.Select(x => (byte)x).ToArray()
                            };
                            context.ScheduleUploadedFiles.Add(new ScheduleUploadedFile()
                            {
                                Schedule = schedule,
                                UploadedFile = uploadedFile
                            });
                        }
                    }

                    status = true;
                }
                else
                    status = false;
            }
            context.SaveChanges();

            return new JsonResult { Data = new { status }};
        }

        [HttpPost]
        public JsonResult DeleteEvent(int scheduleID)
        {
            bool status = false;

            var accountSchedule = context.Schedules.Where(x => x.ScheduleID == scheduleID).FirstOrDefault();

            if (accountSchedule != null)
            {
                context.Schedules.Remove(accountSchedule);
                context.SaveChanges();

                status = true;
            }

            return new JsonResult { Data = new { status } };
        }

        [HttpGet]
        public JsonResult GetAllEventsToManage()
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();

            var allEventTypes = context.EventTypes.Where(x => x.UserID == null).ToList();
            allEventTypes.AddRange(context.EventTypes.Where(z => z.UserID == userAcc.UserID).ToList());

            //SelectList listOfAllEventTypes = new SelectList(allEventTypes, "EventTypeID", "EventName");
            //ViewBag.ListOfEventTypes = listOfAllEventTypes;

            return new JsonResult { Data = allEventTypes, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult DeleteEventType(int deleteEventTypeID)
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();
            bool status = false;

            var eventTypeToDelete = context.EventTypes.Find(deleteEventTypeID);

            if (eventTypeToDelete != null)
            {
                if (eventTypeToDelete.UserID == userAcc.UserID)
                {
                    var userEventsWithCurrentEventType = context.Schedules
                        .Where(x => x.UserID == userAcc.UserID && x.EventTypeID == deleteEventTypeID).ToList();
                    var eventTypeOther = context.EventTypes.Where(x => x.EventName == "Inne").FirstOrDefault();

                    if (userEventsWithCurrentEventType != null)
                        foreach (var eventToModify in userEventsWithCurrentEventType)
                        {
                            eventToModify.EventTypeID = eventTypeOther.EventTypeID;
                            context.Entry(eventToModify).State = System.Data.Entity.EntityState.Modified;
                        }

                    context.EventTypes.Remove(eventTypeToDelete);
                    context.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status } };
        }

        [HttpPost]
        public JsonResult SaveEventType(EventType eventType)
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();
            bool status = false;

            if (eventType.EventName == null)
                return new JsonResult { Data = new { status } };

            var checkExistingEventByID = context.EventTypes.Find(eventType.EventTypeID);
            var checkExistingEventTypes = context.EventTypes
                .Where(x => x.UserID == userAcc.UserID && x.EventName == eventType.EventName).FirstOrDefault();

            if (checkExistingEventByID != null)
            {
                if (checkExistingEventByID.UserID != null)
                {
                    if (checkExistingEventTypes != null)
                    {
                        if (checkExistingEventByID.EventTypeID == checkExistingEventTypes.EventTypeID)
                        {
                            checkExistingEventByID.EventName = eventType.EventName;
                            checkExistingEventByID.EventColor = eventType.EventColor;
                            context.Entry(checkExistingEventByID).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();
                            status = true;
                        }
                    }
                    else
                    {
                        checkExistingEventByID.EventName = eventType.EventName;
                        checkExistingEventByID.EventColor = eventType.EventColor;
                        context.Entry(checkExistingEventByID).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                        status = true;
                    }
                }
            }
            else
            {
                if (checkExistingEventTypes == null)
                {
                    context.EventTypes.Add(new EventType()
                    {
                        UserID = userAcc.UserID,
                        EventName = eventType.EventName,
                        EventColor = eventType.EventColor
                    });
                    context.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status } };
        }

        public FileResult FileDownload(int? fileID)
        {
            var findFile = context.UploadedFiles.Find(fileID);

            return File(findFile.FileContent, findFile.ContentType, findFile.FileName);
        }

        public JsonResult FileRemove(int? fileID)
        {
            var findFile = context.UploadedFiles.Find(fileID);

            if (findFile != null)
            {
                context.UploadedFiles.Remove(findFile);
            }

            context.SaveChanges();

            return new JsonResult { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [HttpGet]
        public ActionResult Import()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Import(HttpPostedFileBase importingFile, bool importFromToday = false)
        {
            if (importingFile == null)
            {
                ViewBag.ChoiseFile = "Proszę wybrać plik";
                return View();
            }

            (var loginAcc, var userAcc) = GetInfoFromCookie();

            var eventTypeOthers = context.EventTypes.Where(x => x.EventName == "Inne").FirstOrDefault();

            bool realize = false;

            if (importingFile.ContentLength > 0)
            {
                byte[] binData = new byte[importingFile.ContentLength];

                if (importingFile.FileName.EndsWith(".csv"))
                {
                    await importingFile.InputStream.ReadAsync(binData, 0, importingFile.ContentLength);

                    string result = Encoding.UTF8.GetString(binData);
                
                    CSVDataSaveToDatabase(CSVFileExecute(result), userAcc, eventTypeOthers, importFromToday);

                    if (ViewBag.ErrorColumns != null)
                        return View();

                    return RedirectToAction("Schedule");
                }
                else if (importingFile.FileName.EndsWith(".ics"))
                {
                    bool redundantRecord = false;
                    await importingFile.InputStream.ReadAsync(binData, 0, importingFile.ContentLength);

                    var path = Path.Combine(Server.MapPath("~/App_Data/Temp"), importingFile.FileName);

                    FileStream streamWriter = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true);
                    await streamWriter.WriteAsync(binData, 0, importingFile.ContentLength);
                    streamWriter.Dispose();

                    // Load from stream of binData without save inside temp folder in server (delete 4 previous lines and 2 last)
                    // which contains FileInfo
                    //MemoryStream memory = new MemoryStream(binData);
                    //IICalendarCollection calendars = Ical.Net.Calendar.LoadFromStream(memory);

                    IICalendarCollection calendars = Ical.Net.Calendar.LoadFromFile(path);
                    IList<Occurrence> occurrences = null;

                    if (importFromToday)
                    {
                        // Get all events that occur today.
                        occurrences = calendars.GetOccurrences(DateTime.Today, DateTime.Today.AddYears(100)).ToList();
                    }
                    else
                    {
                        // Get all occurrences for the period from 100 years ago and 100 years later.
                        occurrences = calendars.GetOccurrences(DateTime.Today.AddYears(-100), DateTime.Today.AddYears(100)).ToList();
                    }

                    // Iterate through each occurrence and display information about it

                    foreach (Occurrence occurrence in occurrences)
                    {
                        IRecurringComponent rc = (IRecurringComponent) occurrence.Source;
                        if (rc != null)
                        {
                            if (occurrence.Period.EndTime.AsSystemLocal < DateTime.Now)
                                realize = true;
                            else
                                realize = false;

                            var checkRedundantRecord = context.Schedules
                                .Where(x => x.UserID == userAcc.UserID && x.Subject == rc.Summary && 
                                x.StartDate == occurrence.Period.StartTime.AsSystemLocal && 
                                x.EndDate == occurrence.Period.EndTime.AsSystemLocal && x.Description == rc.Description).FirstOrDefault();

                            if (checkRedundantRecord == null)
                            {
                                context.Schedules.Add(new Schedule()
                                {
                                    UserID = userAcc.UserID,
                                    EventTypeID = eventTypeOthers.EventTypeID,
                                    Subject = rc.Summary,
                                    StartDate = occurrence.Period.StartTime.AsSystemLocal,
                                    EndDate = occurrence.Period.EndTime.AsSystemLocal,
                                    Description = rc.Description,
                                    Realization = realize
                                });
                            }
                            else
                            {
                                if (!redundantRecord)
                                {
                                    TempData["RedundantRecord"] = "Pominięto powtarzające się rekordy";
                                    redundantRecord = true;
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                    
                    FileInfo fileInfo = new FileInfo(path);
                    fileInfo.Delete();

                    return RedirectToAction("Schedule");
                }
                else
                {
                    ViewBag.WrongFileExtension = "Zły format pliku dopuszczalne: .csv oraz .ics";
                    return View();
                }
            }
            else
                return View();
        }

        public ActionResult Export()
        {
            return View();
        }

        public FileContentResult ICSFileExport()
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();

            var schedulesAcc = context.Schedules.Where(x => x.UserID == userAcc.UserID).ToList();

            string fileName = loginAcc.Email + ".ics";

            Ical.Net.Calendar iCal = new Ical.Net.Calendar();

            foreach (var record in schedulesAcc)
            {
                Ical.Net.Event eventRecord = iCal.Create<Ical.Net.Event>();

                eventRecord.Start = new CalDateTime(record.StartDate);
                eventRecord.End = new CalDateTime(record.EndDate);
                eventRecord.Description = record.Description;
                eventRecord.Summary = record.Subject;
            }

            CalendarSerializer calendarSerializer = new CalendarSerializer();
            string contentICSFile = calendarSerializer.SerializeToString(iCal);

            return File(new UTF8Encoding().GetBytes(contentICSFile), "text/calendar", fileName);
        }

        public FileContentResult CSVFileExport()
        {
            (var loginAcc, var userAcc) = GetInfoFromCookie();

            string fileDownloadName = loginAcc.Email + ".csv";

            var schedulesAcc = context.Schedules.Where(x => x.UserID == userAcc.UserID).ToList();

            StringBuilder contentCSVFile = new StringBuilder("Subject,Start Date,Start Time,End Date,End Time,Description\r\n");

            foreach (var record in schedulesAcc)
            {
                if (record.Subject.Contains(','))
                    contentCSVFile.Append("\"" + record.Subject + "\",");
                else
                    contentCSVFile.Append(record.Subject + ",");

                contentCSVFile.Append(record.StartDate.Date.ToString("MM/dd/yyyy") + "," + 
                    record.StartDate.ToString("h:mm tt") + ",");
                contentCSVFile.Append(record.EndDate.Date.ToString("MM/dd/yyyy") + "," +
                    record.EndDate.ToString("h:mm tt") + ",");

                if (record.Description != null)
                {
                    if (record.Description.Contains(','))
                        contentCSVFile.Append("\"" + record.Description + "\"");
                    else
                        contentCSVFile.Append(record.Description);
                }
                else
                    contentCSVFile.Append(record.Description);

                contentCSVFile.Append("\r\n");
            }

            return File(new System.Text.UTF8Encoding().GetBytes(contentCSVFile.ToString()), "text/csv", fileDownloadName);
        }


        private DataTable CSVFileExecute(string result)
        {
            DataTable csvData = new DataTable();

            string[] tempSplitNewLine = result.Trim().Split('\n');
            string[] rows = new string[tempSplitNewLine.Length];

            //string[] seps = { "\",", ",\"" };
            string[] seps = { "\",\"" };
            char[] quotes = { '\"', ' ' };
            string[] colFields = null;
            for (int currentRow = 0; currentRow < tempSplitNewLine.Length; currentRow++)
            {
                if (currentRow != tempSplitNewLine.Length - 1 && !tempSplitNewLine[currentRow].EndsWith("\r"))
                {
                    rows[currentRow] = tempSplitNewLine[currentRow] + "\n" + tempSplitNewLine[currentRow + 1];
                    rows[currentRow] = rows[currentRow].Split('\r').First();

                    var removedSplitRecord = tempSplitNewLine.ToList();
                    removedSplitRecord.RemoveAt(currentRow + 1);
                    tempSplitNewLine = removedSplitRecord.ToArray();
                }
                else
                {
                    rows[currentRow] = tempSplitNewLine[currentRow].Split('\r').First();
                }

                var fields = rows[currentRow]
                    .Split(seps, StringSplitOptions.None)
            //.Select(s => s.Trim(quotes).Replace("\\\"", "\""))
                    .Select(s => s.Trim(quotes).Replace("\"\"",""))
                    .ToArray();

                if (colFields == null)
                {
                    colFields = fields[0].Split(',');

                    if (colFields[0].First() == 65279)
                    {
                        colFields[0] = colFields[0].Remove(0,1);
                    }

                    if (colFields[0] == "Subject" && colFields[1] == "Start Date")
                    {
                        foreach (string column in colFields)
                        {
                            DataColumn datacolumn = new DataColumn(column);
                            datacolumn.AllowDBNull = true;
                            csvData.Columns.Add(datacolumn);
                        }
                    }
                    else
                    {
                        ViewBag.ErrorColumns = "Pierwsza kolumna musi być Subject, a druga Start Date";
                        break;
                    }
                }
                else
                {
                    DataRow csvRow = csvData.NewRow();
                    int index = 0;

                    var splitedSlashRows = fields[0].Split('\"');
                    string[] splittedCommaRows = null;

                    if (splitedSlashRows.Length == 1)
                    {
                        splittedCommaRows = splitedSlashRows[0].Split(',');
                        for (index = 0; index < splittedCommaRows.Length; index++)
                            csvRow[index] = splittedCommaRows[index];
                    }
                    else if (splitedSlashRows.Length == 2)
                    {
                        if (rows[currentRow].StartsWith("\""))
                        {
                            int indexOfRows = 0;
                            splittedCommaRows = splitedSlashRows[1].Split(',');    //dane od 1-5
                            for (index = 0; index < splittedCommaRows.Length; index++)
                            {
                                if (splittedCommaRows[index] == "")
                                    continue;
                                csvRow[indexOfRows + 1] = splittedCommaRows[index];
                                indexOfRows++;
                            }
                            csvRow[0] = splitedSlashRows[0];
                        }
                        else
                        {
                            int indexOfRows = 0;
                            splittedCommaRows = splitedSlashRows[0].Split(',');   //dane od 0-4
                            for (index = 0; index < splittedCommaRows.Length; index++)
                            {
                                if (splittedCommaRows[index] == "")
                                    continue;
                                csvRow[indexOfRows] = splittedCommaRows[index];
                                indexOfRows++;
                            }
                            csvRow[csvData.Columns.Count -1] = splitedSlashRows[1];
                        }
                    }
                    else if (splitedSlashRows.Length == 3)
                    {
                        splittedCommaRows = splitedSlashRows[1].Split(','); //dane od 1-3 lub 1-4
                        if (fields.Length == 2)
                        {
                            int indexOfRows = 0;
                            for (int i = 0; i < splittedCommaRows.Length; i++)
                            {
                                if (splittedCommaRows[i] == "")
                                    continue;
                                csvRow[indexOfRows + 1] = splittedCommaRows[i];
                                indexOfRows++;
                            }
                            csvRow[0] = splitedSlashRows[0];
                            csvRow[csvData.Columns.Count - 2] = splitedSlashRows[2];
                            csvRow[csvData.Columns.Count - 1] = fields[1];
                        }
                        else
                        {
                            int indexOfRows = 0;
                            for (index = 0; index < splittedCommaRows.Length; index++)
                            {
                                if (splittedCommaRows[index] == "")
                                    continue;
                                csvRow[indexOfRows + 1] = splittedCommaRows[index];
                                indexOfRows++;
                            }
                            csvRow[0] = splitedSlashRows[0];
                            csvRow[csvData.Columns.Count -1] = splitedSlashRows[2];
                        }
                    }
                    csvData.Rows.Add(csvRow);
                }
            }

            return csvData;
        }

        private void CSVDataSaveToDatabase(DataTable csvData, Calendar.User user, Calendar.EventType eventType, bool importFromToday)
        {
            const int subjectColumnIndex = 0;
            const int startDateColumnIndex = 1;
            int? startTimeColumnIndex = null;
            int? endDateColumnIndex = null;
            int? endTimeColumnIndex = null;
            int? descriptionColumnIndex = null;

            bool redundantRecord = false;

            foreach (DataColumn column in csvData.Columns)
            {
                if (column.ColumnName == "Start Time")
                    startTimeColumnIndex = column.Ordinal;
                if (column.ColumnName == "End Date")
                    endDateColumnIndex = column.Ordinal;
                if (column.ColumnName == "End Time")
                    endTimeColumnIndex = column.Ordinal;
                if (column.ColumnName == "Description")
                    descriptionColumnIndex = column.Ordinal;
            }

            foreach (DataRow row in csvData.Rows)
            {
                bool realize = false;
                string description = null;
                string subject = row.ItemArray[subjectColumnIndex].ToString();

                DateTime startDate = DateTime.ParseExact(row.ItemArray[startDateColumnIndex].ToString(), "MM/dd/yyyy", null);

                if (importFromToday)
                {
                    if (startDate < DateTime.Today)
                        continue;
                }

                DateTime endDate = new DateTime();

                if (startTimeColumnIndex != null)
                    startDate = startDate + Convert.ToDateTime(row.ItemArray[startTimeColumnIndex.Value]).TimeOfDay;
                else
                    startDate = startDate + DateTime.Now.TimeOfDay;

                if (endDateColumnIndex != null && endTimeColumnIndex != null)
                {
                    endDate = DateTime.ParseExact(row.ItemArray[endDateColumnIndex.Value].ToString(), "MM/dd/yyyy", null);
                    endDate = endDate + Convert.ToDateTime(row.ItemArray[endTimeColumnIndex.Value]).TimeOfDay;
                }
                else
                {
                    if (endDateColumnIndex == null)
                    {
                        endDate = startDate.Date;
                    }
                    else
                        endDate = DateTime.ParseExact(row.ItemArray[endDateColumnIndex.Value].ToString(), "MM/dd/yyyy", null);

                    if (endTimeColumnIndex == null)
                    {
                        endDate = endDate + endDate.AddMinutes(30).TimeOfDay;
                    }
                    else
                        endDate = endDate + Convert.ToDateTime(row.ItemArray[endTimeColumnIndex.Value]).TimeOfDay;
                }

                if (descriptionColumnIndex != null)
                    description = row.ItemArray[descriptionColumnIndex.Value].ToString();

                if (endDate < DateTime.Today)
                    realize = true;
                else
                    realize = false;

                var checkRedundantRecord = context.Schedules
                    .Where(x => x.UserID == user.UserID && x.Subject == subject && x.StartDate == startDate && x.EndDate == endDate && 
                    x.Description == description).FirstOrDefault();

                if (checkRedundantRecord == null)
                {
                    context.Schedules.Add(new Schedule()
                    {
                        UserID = user.UserID,
                        EventTypeID = eventType.EventTypeID,
                        Subject = subject,
                        StartDate = startDate,
                        EndDate = endDate,
                        Description = description,
                        Realization = realize
                    });
                }
                else
                {
                    if (!redundantRecord)
                    {
                        TempData["RedundantRecord"] = "Pominięto powtarzające się rekordy";
                        redundantRecord = true;
                    }
                }
            }
            context.SaveChanges();
        }

        private (Calendar.Account, Calendar.User) GetInfoFromCookie()
        {
            string loginCookie = HttpContext.User.Identity.Name;
            var loginAcc = context.Accounts.Where(x => x.Email == loginCookie).FirstOrDefault();
            var userAcc = context.Users.Where(x => x.AccountID == loginAcc.AccountID).FirstOrDefault();

            return (loginAcc, userAcc);
        }

    }
}